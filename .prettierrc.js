export default {
  trailingComma: 'none',
  singleQuote: true,
  semi: true
};
