import Elysia, { t } from 'elysia';
import { cors } from '@elysiajs/cors';
import { html } from '@elysiajs/html';
import staticPlugin from '@elysiajs/static';

const elysia = new Elysia()
  .use(cors())
  .use(html({ autoDetect: true }))
  .use(staticPlugin());

let baseWorkTime: number = 0;
let baseFreetime: number = 0;
let worktime: number = 0;
let freetime: number = 0;
let mode: 'worktime' | 'freetime' = 'worktime';

elysia.get('/', () => {
  return Bun.file('./timer.html');
});

elysia.delete('/timer', ({ html }) => {
  worktime = 0;
  freetime = 0;
  return html(
    <main id="main">
      <div class="form-container">
        <form
          class="timer-form"
          id="timer-form"
          hx-post="http://localhost:3000/timer"
          hx-swap="outerHTML"
          hx-target="#timers"
        >
          <label id="worktime">Kaç dakika çalışmak istiyorsun?</label>
          <input type="number" id="worktime" name="worktime" />
          <label id="freetime">Kaç dakika dinlenmek istiyorsun?</label>
          <input type="number" id="freetime" name="freetime" />
          <button type="submit" class="start">
            Başlat
          </button>
          <button
            type="button"
            hx-delete="http://localhost:3000/timer"
            hx-target="#main"
            class="reset"
          >
            Sıfırla
          </button>
        </form>
      </div>
      <ul id="timers">
        <li id="worktime" hx-select="#worktime" hx-swap="outerHTML"></li>
        <li id="freetime" hx-select="#freetime" hx-swap="outerHTML"></li>
      </ul>
    </main>
  );
});

elysia.post(
  '/timer',
  ({ body, html }) => {
    baseWorkTime = Math.floor(parseInt(body.worktime) * 60);
    worktime = Math.floor(parseInt(body.worktime) * 60);
    baseFreetime = Math.floor(parseInt(body.freetime) * 60);
    freetime = Math.floor(parseInt(body.freetime) * 60);
    console.log(baseWorkTime, baseFreetime);
    return html(
      <ul
        id="timers"
        hx-get="http://localhost:3000/timer"
        hx-swap="outerHTML"
        hx-trigger="every 1s"
      >
        <li hx-select="#worktime" id="worktime" safe>
          {`${(baseWorkTime % 3600) / 60 < 10 ? `0${Math.floor((baseWorkTime % 3600) / 60)}` : `${(baseWorkTime % 3600) / 60}`}:${baseWorkTime % 60 < 10 ? `0${Math.floor(baseWorkTime % 60)}` : `${Math.floor(baseWorkTime % 60)}`}`}
        </li>
        <li
          id="freetime"
          hx-select="#freetime"
          safe
        >{`${(baseFreetime % 3600) / 60 < 10 ? `0${Math.floor((baseFreetime % 3600) / 60)}` : `${(baseFreetime % 3600) / 60}`}:${baseFreetime % 60 < 10 ? `0${Math.floor(baseFreetime % 60)}` : `${Math.floor(baseFreetime % 60)}`}`}</li>
      </ul>
    );
  },
  {
    body: t.Object({ worktime: t.String(), freetime: t.String() })
  }
);

elysia.get('/timer', ({ html }) => {
  debugger;
  let timers: JSX.Element | undefined;
  if (mode === 'worktime') {
    worktime -= 1;
    if (worktime === 0) {
      mode = 'freetime';
      freetime = baseFreetime;
      timers = (
        <ul
          id="timers"
          hx-get="http://localhost:3000/timer"
          hx-swap="outerHTML"
          hx-trigger="every 1s"
        >
          <li id="worktime" hx-select="#worktime">
            00:00
          </li>
          <li hx-select="#freetime" id="freetime" safe>
            {`${(baseFreetime % 3600) / 60 < 10 ? `0${Math.floor(baseFreetime % 3600) / 60}` : `${Math.floor((baseWorkTime % 3600) / 60)}`}:${baseWorkTime % 60 < 10 ? `0${Math.floor(baseFreetime % 60)}` : `${Math.floor(baseFreetime % 60)}`}`}
          </li>
        </ul>
      );
    } else {
      timers = (
        <ul
          id="timers"
          hx-get="http://localhost:3000/timer"
          hx-swap="outerHTML"
          hx-trigger="every 1s"
        >
          <li id="worktime" hx-select="#worktime" safe>
            {`${(worktime % 3600) / 60 < 10 ? `0${Math.floor((worktime % 3600) / 60)}` : `${Math.floor((worktime % 3600) / 60)}`}:${worktime % 60 < 10 ? `0${Math.floor(worktime % 60)}` : `${Math.floor(worktime % 60)}`}`}
          </li>
          <li id="freetime" hx-select="#freetime" safe>
            {`${(freetime % 3600) / 60 < 10 ? `0${Math.floor((freetime % 3600) / 60)}` : `${Math.floor((freetime % 3600) / 60)}`}:${freetime % 60 < 10 ? `0${Math.floor(freetime % 60)}` : `${Math.floor(freetime % 60)}`}`}
          </li>
        </ul>
      );
    }
  } else if (mode === 'freetime') {
    freetime -= 1;
    if (freetime === 0) {
      mode = 'worktime';
      worktime = baseWorkTime;
      timers = (
        <ul
          id="timers"
          hx-get="http://localhost:3000/timer"
          hx-swap="outerHTML"
          hx-trigger="every 1s"
        >
          <li hx-select="#worktime" id="worktime" safe>
            {`${(baseWorkTime % 3600) / 60 < 10 ? `0${Math.floor(baseWorkTime % 3600) / 60}` : `${Math.floor((baseWorkTime % 3600) / 60)}`}:${baseWorkTime % 60 < 10 ? `0${Math.floor(baseWorkTime % 60)}` : `${Math.floor(baseWorkTime % 60)}`}`}
          </li>
          <li id="freetime" hx-select="#freetime">
            00:00
          </li>
        </ul>
      );
    } else {
      timers = (
        <ul
          id="timers"
          hx-get="http://localhost:3000/timer"
          hx-swap="outerHTML"
          hx-trigger="every 1s"
        >
          <li id="worktime" hx-select="#worktime" safe>
            {`${(worktime % 3600) / 60 < 10 ? `0${Math.floor((worktime % 3600) / 60)}` : `${Math.floor((worktime % 3600) / 60)}`}:${worktime % 60 < 10 ? `0${Math.floor(worktime % 60)}` : `${Math.floor(worktime % 60)}`}`}
          </li>
          <li id="freetime" hx-select="#freetime" safe>
            {`${(freetime % 3600) / 60 < 10 ? `0${Math.floor((freetime % 3600) / 60)}` : `${Math.floor((freetime % 3600) / 60)}`}:${freetime % 60 < 10 ? `0${Math.floor(freetime % 60)}` : `${Math.floor(freetime % 60)}`}`}
          </li>
        </ul>
      );
    }
  }

  return html(timers!);
});

elysia.listen(3000, () =>
  console.log('Timer is listening on port http://localhost:3000')
);
